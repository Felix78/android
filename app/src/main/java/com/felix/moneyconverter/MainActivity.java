package com.felix.moneyconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final Button convertButton = findViewById(R.id.buttonConvert);
        convertButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                final EditText textToConvert = findViewById(R.id.textInput);
                String inputText = textToConvert.getText().toString();
                float number = Float.parseFloat(inputText);
                final TextView textToDisplay = findViewById(R.id.resultTextView);
                number = number * 1.08f;
                textToDisplay.setText(number + "$");
            }
        });

        final Button btnInfo = findViewById(R.id.AproposBtn);
        btnInfo.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                Intent intent = new Intent(MainActivity.this , AboutActivity.class);
                startActivity(intent);
            }
        });


        Log.i("MainActivity", "Hello World !");
    }
}
